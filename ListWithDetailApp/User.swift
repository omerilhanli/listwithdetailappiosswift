//
//  User.swift
//  ListWithDetailApp
//
//  Created by Ömer İlhanlı on 2.05.2019.
//  Copyright © 2019 Ömer İlhanlı. All rights reserved.
//

import Foundation
import UIKit

class User {
    
    var userName: String
    var userPhoto: UIImage
    
    init(userName: String, userPhoto: UIImage) {
        self.userName = userName
        self.userPhoto = userPhoto
    }
}
