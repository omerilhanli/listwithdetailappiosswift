//
//  DetailViewController.swift
//  ListWithDetailApp
//
//  Created by Ömer İlhanlı on 2.05.2019.
//  Copyright © 2019 Ömer İlhanlı. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    var user: User?
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageViewPhoto: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelName.text = user?.userName
        imageViewPhoto.image = user?.userPhoto
    }
    
    @IBAction func returnBackVC(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
        // return back.
    }
    
}
