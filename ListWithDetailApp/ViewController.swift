import UIKit

class ViewController: UIViewController {

    var users: [User] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        users = create()
    }
    
    private func create() -> [User] {
        
        let user01 = User(userName: "Cem Yılmaz", userPhoto: UIImage(named: "1-cmylmz")!)
        let user02 = User(userName: "Okan Bayülgen", userPhoto: UIImage(named: "2-knbylgn")!)
        let user03 = User(userName: "Beyazıt Öztürk", userPhoto: UIImage(named: "3-byztztrk")!)
        let user04 = User(userName: "Yılmaz Erdoğan", userPhoto: UIImage(named: "4-ymzrdgn")!)
        
        return [user01,user02,user03,user04]
    }
    
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return cell(tableView, indexPath)
    }
    
    private func cell(_ tableView: UITableView, _ indexPath: IndexPath) -> UserCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! UserCell
        
        let user: User = users[indexPath.row]
        
        cell.bind(user)
        
        return cell
    }
}
