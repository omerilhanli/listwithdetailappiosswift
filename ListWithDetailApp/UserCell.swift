
import Foundation
import UIKit

class UserCell: UITableViewCell {
    
    @IBOutlet weak var userPhotoImageView: UIImageView!
    
    @IBOutlet weak var userNameLabel: UILabel!
    
    var user: User?
    
    func bind(_ user: User){
        self.user = user
        userNameLabel.text = user.userName
        userPhotoImageView.image = user.userPhoto
    }
    
    @IBAction func openDetailScreen(_ sender: UIButton) {
      
        open()
    }
    
    private func open(){
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        let viewController = storyboard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        
        viewController.user = user
        
        parentViewController?.present(viewController, animated: true, completion: nil)
    }
}

extension UserCell {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if parentResponder is UIViewController {
                return parentResponder as! UIViewController?
            }
        }
        return nil
    }
}
